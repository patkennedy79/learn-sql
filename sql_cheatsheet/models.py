from abc import ABC, abstractmethod

from prettytable import from_db_cursor


# -------------------
# Abstract Base Class
# -------------------
class Model(ABC):
    """Abstract Base Class (ABC) for a table in the PostgreSQL database."""

    def __init__(self, table_name: str, db_cursor):
        self.table_name = table_name
        self.db_cursor = db_cursor

    @abstractmethod
    def create_table_schema(self):
        """Create the schema for the table."""
        pass

    @abstractmethod
    def add_new_record(self, elements: list):
        """Add a new record to the table."""
        pass

    def drop_table(self):
        """Drop the table."""
        self.db_cursor.execute(f'DROP TABLE IF EXISTS {self.table_name};')

    def add_column(self, column_name: str, column_type: str):
        """Add a new column to the table."""
        self.db_cursor.execute(f"""
            ALTER TABLE {self.table_name}
            ADD {column_name} {column_type};
        """)

    def delete_column(self, column_name: str):
        """Delete a column from the table."""
        self.db_cursor.execute(f"""
            ALTER TABLE {self.table_name}
            DROP COLUMN {column_name};
        """)

    def delete_record(self, index: int):
        """Delete the specified record from the table."""
        self.db_cursor.execute(f"""
            DELETE FROM {self.table_name}
            WHERE id = {index};
        """)

    def print_all_records(self):
        """Print all the records in the table."""
        print(f'\n{self.table_name}:')
        self.db_cursor.execute(f"""
            SELECT *
            FROM {self.table_name};
        """)
        table = from_db_cursor(self.db_cursor)
        print(table)

    def print_table_schema(self):
        """Print the schema for the table."""
        print(f'\n{self.table_name} table schema:')
        self.db_cursor.execute(f"""
            SELECT column_name, data_type
            FROM information_schema.columns
            WHERE table_name = '{self.table_name}';
        """)
        table = from_db_cursor(self.db_cursor)
        print(table)

    def add_records_from_file(self, filename: str):
        """Add records by reading from a file."""
        with open(filename) as f:
            lines = [line for line in f]
            for line in lines:
                elements = line.strip().split(',')
                self.add_new_record(elements)


# ---------------
# Database Tables
# ---------------

class OlympicHost(Model):
    """Class for working with the olympic_hosts table in the PostgreSQL database."""

    def __init__(self, db_cursor):
        super().__init__('olympic_hosts', db_cursor)

    def create_table_schema(self):
        """Create the schema for the olympic_hosts table."""

        # Create the schema (i.e. columns) for the products table
        # NOTE: The keyword SERIAL is used in PostgreSQL for an
        #       auto-incrementing primary key.  In other relational
        #       database systems, this would be "PRIMARY KEY AUTO_INCREMENT".
        self.db_cursor.execute("""
            CREATE TABLE olympic_hosts(
                id SERIAL PRIMARY KEY,
                city VARCHAR(255) NOT NULL,
                year INT NOT NULL,
                season VARCHAR(50) NOT NULL,
                continent VARCHAR(100) NOT NULL,
                country_id INT,
                CONSTRAINT fk_country FOREIGN KEY(country_id) REFERENCES countries(c_id) ON DELETE SET NULL
            );
        """)

    def add_new_record(self, elements: list):
        """Add a new record to the table."""
        print(elements)
        self.db_cursor.execute(f"""
            INSERT INTO olympic_hosts(city, year, season, continent, country_id)
            VALUES ('{elements[0]}', {elements[1]}, '{elements[2]}', '{elements[3]}', {elements[4]});
        """)


class Country(Model):
    """Class for working with the countries table in the PostgreSQL database."""

    def __init__(self, db_cursor):
        super().__init__('countries', db_cursor)

    def create_table_schema(self):
        """Create the schema for the countries table."""

        # Create the schema (i.e. columns) for the products table
        # NOTE: The keyword SERIAL is used in PostgreSQL for an
        #       auto-incrementing primary key.  In other relational
        #       database systems, this would be "PRIMARY KEY AUTO_INCREMENT".
        self.db_cursor.execute("""
            CREATE TABLE countries(
                c_id SERIAL PRIMARY KEY,
                country VARCHAR(255) NOT NULL,
                capital VARCHAR(255) NOT NULL
            );
        """)

    def add_new_record(self, elements: list):
        """Add a new record to the table."""
        self.db_cursor.execute(f"""
            INSERT INTO countries(country, capital)
            VALUES ('{elements[0]}', '{elements[1]}');
        """)
