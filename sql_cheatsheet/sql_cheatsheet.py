from database import DatabaseContextManager
from models import Country, OlympicHost


# ----------------------
# Database Configuration
# ----------------------
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'learn-sql'
DATABASE_USER = 'postgres'
DATABASE_PASSWORD = 'tomatoes'


# ------------
# Main Program
# ------------

if __name__ == '__main__':
    with DatabaseContextManager(DATABASE_HOST, DATABASE_NAME, DATABASE_USER, DATABASE_PASSWORD) as db:
        db.print_database_version()

        # Create the models
        hosts = OlympicHost(db.db_cursor)
        countries = Country(db.db_cursor)

        # Drop all the tables
        hosts.drop_table()
        countries.drop_table()

        # Create the 'countries' table
        #
        # NOTE: The 'country_capitals_data.csv' file does not contain a record for Canada,
        #       to support showing the different types of JOINs.
        countries.create_table_schema()
        countries.add_records_from_file('sql_cheatsheet/country_capitals_data.csv')

        # Create the `olympics_hosts` table
        #
        # NOTE: The 'olympic_hosts_data.csv' file does not have a country_id for
        #       the Salt Lake City 2002 Olympics, to support showing the different
        #       types of JOINs.
        hosts.create_table_schema()
        hosts.add_records_from_file('sql_cheatsheet/olympic_hosts_data.csv')

        # -------
        # QUERIES
        # -------

        # Retrieve all columns from a single table:
        db.execute_sql_query("""
            SELECT *
            FROM olympic_hosts;
        """)

        # Retrieve specific columns only:
        db.execute_sql_query("""
             SELECT city, year, continent
             FROM olympic_hosts;
         """)

        # -----
        # ALIAS
        # -----

        # Change the column name in the results:
        db.execute_sql_query("""
            SELECT city AS host_city, year
            FROM olympic_hosts;
        """)

        # ---------
        # FILTERS
        # ---------

        # Retrieve all Olympics hosted in Australia:
        db.execute_sql_query("""
            SELECT *
            FROM olympic_hosts
            WHERE continent = 'Australia';
        """)

        # Retrieve all Olympics not hosted in Europe:
        db.execute_sql_query("""
            SELECT *
            FROM olympic_hosts
            WHERE continent <> 'Europe';
        """)

        # Retrieve 3 Olympics hosted in North America:
        db.execute_sql_query("""
            SELECT *
            FROM olympic_hosts
            WHERE continent = 'North America'
            LIMIT 3;
        """)

        # Retrieve the Olympics hosted in either South America or Australia:
        db.execute_sql_query("""
            SELECT city, continent
            FROM olympic_hosts
            WHERE continent = 'South America' OR continent = 'Australia';
        """)

        # Retrieve the Olympics hosted in Europe after 1980:
        db.execute_sql_query("""
            SELECT *
            FROM olympic_hosts
            WHERE continent = 'Europe' AND year > 1980;
        """)

        # Retrieve the Olympics held between 2000 and 2015:
        db.execute_sql_query("""
            SELECT city, year
            FROM olympic_hosts
            WHERE year BETWEEN 2000 AND 2015;
        """)

        # Retrieve the Olympics hosted in Asia or Europe:
        db.execute_sql_query("""
            SELECT city, continent, year
            FROM olympic_hosts
            WHERE continent IN ('Asia', 'Europe');
        """)

        # Retrieve the Olympic host cities starting with the letter S:
        db.execute_sql_query("""
            SELECT city, continent, year
            FROM olympic_hosts
            WHERE city LIKE 'S%';
        """)

        # Retrieve the Olympics not hosted in the Americas:
        db.execute_sql_query("""
            SELECT city, continent, year
            FROM olympic_hosts
            WHERE continent NOT LIKE '%America';
        """)

        # -------------------
        # Aggregate Functions
        # -------------------

        # Retrieve the number of Olympics hosted in Asia:
        db.execute_sql_query("""
            SELECT COUNT(*)
            FROM olympic_hosts
            WHERE continent = 'Asia';
        """)

        # Retrieve the number of continents that have hosted the Olympics:
        db.execute_sql_query("""
            SELECT COUNT(DISTINCT continent) AS number_of_continents
            FROM olympic_hosts;
        """)

       # Retrieve the average year that Australia hosted the Olympics:
        db.execute_sql_query("""
            SELECT AVG(year) AS avg_year
            FROM olympic_hosts
            WHERE continent = 'Australia';
        """)

        # Retrieve the average year that Asia hosted the Olympics with rounding:
        db.execute_sql_query("""
            SELECT ROUND(AVG(year)) AS avg_year
            FROM olympic_hosts
            WHERE continent = 'Asia';
        """)

        # Retrieve the earliest and most recent years that the Olympics where hosted in North America:
        db.execute_sql_query("""
            SELECT MIN(year), MAX(year)
            FROM olympic_hosts
            WHERE continent = 'North America';
        """)

        # -------
        # SORTING
        # -------

        # Retrieve the Olympics hosted in Europe from oldest to most recent:
        db.execute_sql_query("""
            SELECT city, continent, year
            FROM olympic_hosts
            WHERE continent = 'Europe'
            ORDER BY year;
        """)

        # Retrieve the Olympics hosted in Asia from most recent to oldest:
        db.execute_sql_query("""
            SELECT city, continent, year
            FROM olympic_hosts
            WHERE continent = 'Asia'
            ORDER BY year DESC;
        """)

        # --------
        # Grouping
        # --------

        # Retrieve the number of Olympics hosted on each continent:
        db.execute_sql_query("""
            SELECT continent, COUNT(*)
            FROM olympic_hosts
            GROUP BY continent;
        """)

        # Retrieve the continents that have hosted more than 3 Olympics:
        db.execute_sql_query("""
            SELECT continent, COUNT(*)
            FROM olympic_hosts
            GROUP BY continent
            HAVING COUNT(year) > 3;
        """)

        # -----
        # Joins
        # -----

        # INNER JOIN - Retrieve only the rows where there is a match in both tables:
        db.execute_sql_query("""
            SELECT olympic_hosts.id, olympic_hosts.city, olympic_hosts.year, countries.country
            FROM olympic_hosts
            JOIN countries
            ON olympic_hosts.country_id = countries.c_id
            WHERE olympic_hosts.continent = 'North America';
        """)

        # LEFT JOIN - Retrieve all rows from the left table and matching rows from right table:
        db.execute_sql_query("""
            SELECT olympic_hosts.id, olympic_hosts.city, olympic_hosts.year, countries.country
            FROM olympic_hosts
            LEFT JOIN countries
            ON olympic_hosts.country_id = countries.c_id
            WHERE olympic_hosts.continent = 'North America';
        """)

        # RIGHT JOIN - Retrieve all rows from the right table and the matching rows from the left table:
        db.execute_sql_query("""
            SELECT olympic_hosts.id, olympic_hosts.city, olympic_hosts.year, countries.country
            FROM olympic_hosts
            RIGHT JOIN countries
            ON olympic_hosts.country_id = countries.c_id;
        """)

        # FULL OUTER JOIN - Retrieve all rows where there is a match in either the left or right table
        db.execute_sql_query("""
            SELECT olympic_hosts.id, olympic_hosts.city, olympic_hosts.year, countries.country
            FROM olympic_hosts
            FULL OUTER JOIN countries
            ON olympic_hosts.country_id = countries.c_id;
        """)

        # -------------
        # Set Operators
        # -------------

        # UNION - Combines the results of two or more SELECT statements without duplicates.
        # Retrieve all the cities that hosted the Olympics or are capitals:
        db.execute_sql_query("""
            SELECT olympic_hosts.city
            FROM olympic_hosts
            UNION
            SELECT countries.capital
            FROM countries;
        """)

        # UNION ALL - Keeps the duplicate values that UNION does not:
        db.execute_sql_query("""
            SELECT olympic_hosts.city
            FROM olympic_hosts
            UNION ALL
            SELECT countries.capital
            FROM countries;
        """)

        # INTERSECT - Returns the rows that are in both tables.
        # Retrieve the cities that have hosted the Olympics -AND- are capitals:
        db.execute_sql_query("""
            SELECT olympic_hosts.city
            FROM olympic_hosts
            INTERSECT
            SELECT countries.capital
            FROM countries;
        """)

        # EXCEPT - Returns all the rows in the left table except the ones in the right table.
        # Retrieve the cities that have hosted the Olympics that are not capitals:
        db.execute_sql_query("""
            SELECT olympic_hosts.city
            FROM olympic_hosts
            EXCEPT
            SELECT countries.capital
            FROM countries;
        """)

        # -----------
        # Sub-Queries
        # -----------

        # SQL queries can be nested within another query.
        # Retrieve the cities that hosted the Olympics between 1960 and 1969
        # that are also the capitals of their countries:
        db.execute_sql_query("""
            SELECT c_id, country, capital
            FROM countries
            WHERE capital IN (
                SELECT city
                FROM olympic_hosts
                WHERE year BETWEEN 1960 AND 1969
            );
        """)
