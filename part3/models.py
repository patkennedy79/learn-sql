from abc import ABC, abstractmethod

from prettytable import from_db_cursor


# -------------------
# Abstract Base Class
# -------------------
class Model(ABC):
    """Abstract Base Class (ABC) for a table in the PostgreSQL database."""

    def __init__(self, table_name: str, db_cursor):
        self.table_name = table_name
        self.db_cursor = db_cursor

    @abstractmethod
    def create_table_schema(self):
        """Create the schema for the table."""
        pass

    @abstractmethod
    def add_new_record(self, elements: list):
        """Add a new record to the table."""
        pass

    def drop_table(self):
        """Drop the table."""
        self.db_cursor.execute(f'DROP TABLE IF EXISTS {self.table_name};')

    def add_column(self, column_name: str, column_type: str):
        """Add a new column to the table."""
        self.db_cursor.execute(f"""
            ALTER TABLE {self.table_name}
            ADD {column_name} {column_type};
        """)

    def delete_column(self, column_name: str):
        """Delete a column from the table."""
        self.db_cursor.execute(f"""
            ALTER TABLE {self.table_name}
            DROP COLUMN {column_name};
        """)

    def delete_record(self, index: int):
        """Delete the specified record from the table."""
        self.db_cursor.execute(f"""
            DELETE FROM {self.table_name}
            WHERE id = {index};
        """)

    def print_all_records(self):
        """Print all the records in the table."""
        print(f'\n{self.table_name}:')
        self.db_cursor.execute(f"""
            SELECT *
            FROM {self.table_name};
        """)
        table = from_db_cursor(self.db_cursor)
        print(table)

    def print_table_schema(self):
        """Print the schema for the table."""
        print(f'\n{self.table_name} table schema:')
        self.db_cursor.execute(f"""
            SELECT column_name, data_type
            FROM information_schema.columns
            WHERE table_name = '{self.table_name}';
        """)
        table = from_db_cursor(self.db_cursor)
        print(table)

    def add_records_from_file(self, filename: str):
        """Add records by reading from a file."""
        with open(filename) as f:
            lines = [line for line in f]
            for line in lines:
                elements = line.strip().split(',')
                self.add_new_record(elements)


# ---------------
# Database Tables
# ---------------

class Products(Model):
    """Class for working with the products table in the PostgreSQL database."""

    def __init__(self, db_cursor):
        super().__init__('products', db_cursor)

    def create_table_schema(self):
        """Create the schema for the products table."""

        # Create the schema (i.e. columns) for the products table
        # NOTE: The keyword SERIAL is used in PostgreSQL for an
        #       auto-incrementing primary key.  In other relational
        #       database systems, this would be "PRIMARY KEY AUTO_INCREMENT".
        self.db_cursor.execute("""
            CREATE TABLE products(
                id SERIAL PRIMARY KEY,
                name VARCHAR(255) NOT NULL,
                quantity INT,
                type VARCHAR(50),
                price FLOAT,
                supplier_id INT,
                location_id INT,
                CONSTRAINT fk_supplier FOREIGN KEY(supplier_id) REFERENCES suppliers(s_id) ON DELETE SET NULL,
                CONSTRAINT fk_location FOREIGN KEY(location_id) REFERENCES locations(l_id) ON DELETE SET NULL
            );
        """)

    def add_new_record(self, elements: list):
        """Add a new record to the table."""
        self.db_cursor.execute(f"""
            INSERT INTO products(name, quantity, type, price, supplier_id, location_id)
            VALUES ('{elements[0]}', {elements[1]}, '{elements[2]}', {elements[3]}, {elements[4]}, {elements[5]});
        """)


class Suppliers(Model):
    """Class for working with the suppliers table in the PostgreSQL database."""

    def __init__(self, db_cursor):
        super().__init__('suppliers', db_cursor)

    def create_table_schema(self):
        """Create the schema for the suppliers table."""

        # Create the schema (i.e. columns) for the suppliers table
        # NOTE: The keyword SERIAL is used in PostgreSQL for an
        #       auto-incrementing primary key.  In other relational
        #       database systems, this would be "PRIMARY KEY AUTO_INCREMENT".
        self.db_cursor.execute("""
            CREATE TABLE suppliers(
                s_id SERIAL PRIMARY KEY,
                s_name VARCHAR(255) NOT NULL,
                s_city VARCHAR(100),
                s_state VARCHAR(50)
            );
        """)

    def add_new_record(self, elements: list):
        """Add a new record to the suppliers table."""
        self.db_cursor.execute(f"""
            INSERT INTO suppliers(s_name, s_city, s_state)
            VALUES ('{elements[0]}', '{elements[1]}', '{elements[2]}');
        """)


class Locations(Model):
    """Class for working with the locations table in the PostgreSQL database."""

    def __init__(self, db_cursor):
        super().__init__('locations', db_cursor)

    def create_table_schema(self):
        """Create the schema for the locations table."""

        # Create the schema (i.e. columns) for the locations table
        # NOTE: The keyword SERIAL is used in PostgreSQL for an
        #       auto-incrementing primary key.  In other relational
        #       database systems, this would be "PRIMARY KEY AUTO_INCREMENT".
        self.db_cursor.execute("""
            CREATE TABLE locations(
                l_id SERIAL PRIMARY KEY,
                l_name VARCHAR(255) NOT NULL,
                l_street VARCHAR(100),
                l_city VARCHAR(100),
                l_state VARCHAR(50)
            );
        """)

    def add_new_record(self, elements: list):
        """Add a new record to the locations table."""
        self.db_cursor.execute(f"""
            INSERT INTO locations(l_name, l_street, l_city, l_state)
            VALUES ('{elements[0]}', '{elements[1]}', '{elements[2]}', '{elements[3]}');
        """)
