from database import DatabaseContextManager
from models import Products, Suppliers, Locations


# ----------------------
# Database Configuration
# ----------------------
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'learn-sql'
DATABASE_USER = 'postgres'
DATABASE_PASSWORD = 'tomatoes'


# ------------
# Main Program
# ------------

if __name__ == '__main__':
    with DatabaseContextManager(DATABASE_HOST, DATABASE_NAME, DATABASE_USER, DATABASE_PASSWORD) as db:
        db.print_database_version()

        # Create the models
        products = Products(db.db_cursor)
        suppliers = Suppliers(db.db_cursor)
        locations = Locations(db.db_cursor)

        # Drop all the tables
        products.drop_table()
        suppliers.drop_table()
        locations.drop_table()

        # Create the `suppliers` table schema
        suppliers.create_table_schema()
        suppliers.add_records_from_file('part3/part3_suppliers_data.csv')
        suppliers.print_all_records()

        # Create the `locations` table schema
        locations.create_table_schema()
        locations.add_records_from_file('part3/part3_locations_data.csv')
        locations.print_all_records()

        # Create the `products` table schema
        products.create_table_schema()
        products.add_records_from_file('part3/part3_products_data.csv')
        products.print_all_records()

        # ----------
        # INNER JOIN
        # ----------

        # Print all records from the products table
        db.execute_sql_query("""
            SELECT *
            FROM products;
        """)

        # Print all records from the suppliers table
        db.execute_sql_query("""
            SELECT *
            FROM suppliers;
        """)

        # INNER JOIN the products and suppliers tables
        db.execute_sql_query("""
            SELECT products.id, products.name, products.supplier_id, suppliers.s_id, suppliers.s_name
            FROM products
            JOIN suppliers
            ON products.supplier_id = suppliers.s_id;
        """)

        # ---------
        # LEFT JOIN
        # ---------

        # LEFT JOIN the products and suppliers tables
        db.execute_sql_query("""
            SELECT products.id, products.name, products.supplier_id, suppliers.s_id, suppliers.s_name
            FROM products
            LEFT JOIN suppliers
            ON products.supplier_id = suppliers.s_id;
        """)

        # ----------
        # RIGHT JOIN
        # ----------
        db.execute_sql_query("""
            SELECT products.id, products.name, products.supplier_id, suppliers.s_id, suppliers.s_name
            FROM products
            RIGHT JOIN suppliers
            ON products.supplier_id = suppliers.s_id;
        """)

        # ---------------
        # FULL OUTER JOIN
        # ---------------
        db.execute_sql_query("""
            SELECT products.id, products.name, products.supplier_id, suppliers.s_id, suppliers.s_name
            FROM products
            FULL OUTER JOIN suppliers
            ON products.supplier_id = suppliers.s_id;
        """)

        # -----------------
        # Examples of JOINs
        # -----------------

        # Print all the products with suppliers
        db.execute_sql_query("""
            SELECT products.id, products.name, products.price, suppliers.s_name AS supplier_name
            FROM products
            JOIN suppliers
            ON products.supplier_id = suppliers.s_id;
        """)

        # Print all the products sold at the farmer's market stand on Main Street
        db.execute_sql_query("""
            SELECT locations.l_name AS location_name, locations.l_street, locations.l_city, products.name AS product_name, products.price, products.quantity
            FROM locations
            JOIN products
            ON locations.l_id = products.location_id
            WHERE locations.l_name LIKE 'Main%';
        """)

        # Print all the fruits including where they are supplied from
        db.execute_sql_query("""
            SELECT products.id, products.name AS products_name, suppliers.s_name AS supplier_name, suppliers.s_city, suppliers.s_state
            FROM products
            LEFT JOIN suppliers
            ON products.supplier_id = suppliers.s_id
            WHERE products.type = 'fruit';
        """)

        # Print all the products supplied by the Berry Farm
        db.execute_sql_query("""
            SELECT products.id, products.name AS products_from_berry_farm, products.price, products.quantity, suppliers.s_name AS supplier_name
            FROM products
            JOIN suppliers
            ON products.supplier_id = suppliers.s_id
            WHERE suppliers.s_name = 'Berry Farm'
        """)

        # CHALLENGE PROBLEM - Print the lowest priced item for each type of product in the `products` table
        db.execute_sql_query("""
            SELECT type, name, price
            FROM products
            ORDER BY type;
        """)
        db.execute_sql_query("""
            SELECT type, MIN(price) AS min_price
            FROM products
            GROUP BY type;
        """)
        db.execute_sql_query("""
            SELECT table1.type, table1.name, table1.price
            FROM products AS table1
            JOIN (
                SELECT MIN(price) AS min_price
                FROM products
                GROUP BY type
            ) AS table2
            ON table1.price = table2.min_price;
        """)
        db.execute_sql_query("""
            SELECT type, name, price
            FROM products
            WHERE price IN (
                SELECT MIN(price)
                FROM products
                GROUP BY type
            );
        """)

        db.execute_sql_query("""
            SELECT type
            FROM (
                SELECT *
                FROM products
                WHERE quantity > 10
            )
            GROUP BY type;
        """)

        # -----
        # UNION
        # -----

        db.execute_sql_query("""
            SELECT *
            FROM locations;
        """)
        db.execute_sql_query("""
            SELECT *
            FROM suppliers;
        """)

        # Print the records (based on city and state) that are in both the `locations` and `suppliers` tables,
        # but with the duplicates removed
        db.execute_sql_query("""
            SELECT locations.l_city AS city, locations.l_state AS state
            FROM locations
            UNION
            SELECT suppliers.s_city, suppliers.s_state
            FROM suppliers;
        """)

        # ---------
        # UNION ALL
        # ---------

        # Print all the records from the `locations` and `suppliers` tables by adding a new column in the results
        db.execute_sql_query("""
            SELECT locations.l_city AS city, locations.l_state AS state
            FROM locations
            UNION ALL
            SELECT suppliers.s_city, suppliers.s_state
            FROM suppliers;
        """)
        db.execute_sql_query("""
            SELECT locations.l_city AS city, locations.l_state AS state, 'Location' as type
            FROM locations
            UNION ALL
            SELECT suppliers.s_city, suppliers.s_state, 'Supplier'
            FROM suppliers;
        """)

        # Print all the records from the `locations` and `suppliers` tables using CONCAT()
        db.execute_sql_query("""
            SELECT locations.l_id AS id, locations.l_city AS city, locations.l_state AS state
            FROM locations
            UNION ALL
            SELECT suppliers.s_id, suppliers.s_city, suppliers.s_state
            FROM suppliers;
        """)
        db.execute_sql_query("""
            SELECT CONCAT('l', locations.l_id) AS id, locations.l_city AS city, locations.l_state AS state
            FROM locations
            UNION ALL
            SELECT CONCAT('s', suppliers.s_id), suppliers.s_city, suppliers.s_state
            FROM suppliers;
        """)

        # ---------
        # INTERSECT
        # ---------

        # Find all the locations (by city and state) that are in both the `locations` and `suppliers` tables
        db.execute_sql_query("""
            SELECT locations.l_city AS city, locations.l_state AS state
            FROM locations
            INTERSECT
            SELECT suppliers.s_city, suppliers.s_state
            FROM suppliers;
        """)

        # ------
        # EXCEPT
        # ------

        # Find all the locations (by city and state) that are not in the `suppliers` table
        db.execute_sql_query("""
            SELECT locations.l_city AS city, locations.l_state AS state
            FROM locations
            EXCEPT
            SELECT suppliers.s_city, suppliers.s_state
            FROM suppliers;
        """)

        # -----------
        # Sub-queries
        # -----------

        # Find the average price of the products in the `products` table
        db.execute_sql_query("""
            SELECT AVG(price)
            FROM products;
        """)

        # [Manual approach] Find all the products that are less than the average price of the products in the `products` table
        db.execute_sql_query("""
            SELECT name, price, quantity
            FROM products
            WHERE price < 3.07;
        """)

        # Find all the products that are less than the average price of the products in the `products` table
        db.execute_sql_query("""
            SELECT name, price, quantity
            FROM products
            WHERE price < (
                SELECT AVG(price)
                FROM products
            );
        """)

        # Find all the products grown in Oregon and Washington
        db.execute_sql_query("""
            SELECT name, price, quantity
            FROM products
            WHERE supplier_id IN (
                SELECT s_id
                FROM suppliers
                WHERE s_state IN ('Oregon', 'Washington')
            );
        """)

        db.execute_sql_query("""
            SELECT name, type, price, quantity
            FROM products
            WHERE type = 'fruit';
        """)

        # Example of a nested query with a FROM statement (warning: not an ideal query!)
        db.execute_sql_query("""
            SELECT AVG(fruit_products.price)
            FROM (
                SELECT name, type, price, quantity
                FROM products
                WHERE type = 'fruit'
            ) fruit_products;
        """)

        # Easier way to write the previous query
        db.execute_sql_query("""
            SELECT AVG(price)
            FROM products
            WHERE type = 'fruit';
        """)
