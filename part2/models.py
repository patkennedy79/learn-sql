"""Contains the class for working with the 'products' table in the PostgreSQL database.

The 'Products' class contains the following methods for executing SQL commands related to the 'products' table:
    * `create_table_schema` - creates the 'products' table
    * `drop_table` - drops the 'products' table
    * `add_column` - adds a new column to the 'products' table
    * `delete_column` - deletes an existing column from the 'products' table
    * `add_records_from_file` - reads data from a CSV file and adds each row as a new record in the 'products' table
    * `add_new_record` - adds a new record in the 'products' table
    * `change_product_price` - changes the price of a record in the 'products' table
    * `delete_product` - deletes a record in the 'products' table
    * `print_all_records` - prints all the records in the 'products' table
    * `print_table_schema` - prints the table schema for the 'products' table
    * `get_product_id` - returns the row ID for the specified product name
"""
from prettytable import from_db_cursor


# ---------------
# Database Tables
# ---------------

class Products:
    """Class for working with the products table in the PostgreSQL database."""

    def __init__(self, db_cursor):
        self.db_cursor = db_cursor

    def create_table_schema(self):
        """Create the schema for the products table."""

        # Create the schema (i.e. columns) for the products table
        # NOTE: The keyword SERIAL is used in PostgreSQL for an
        #       auto-incrementing primary key.  In other relational
        #       database systems, this would be "PRIMARY KEY AUTO_INCREMENT".
        self.db_cursor.execute("""
            CREATE TABLE products(
                id SERIAL PRIMARY KEY,
                name VARCHAR(255) NOT NULL,
                quantity INT,
                type VARCHAR(50),
                price FLOAT
            );
        """)

    def drop_table(self):
        """Drop the products table."""
        self.db_cursor.execute('DROP TABLE IF EXISTS products;')

    def add_column(self, column_name: str, column_type: str):
        """Add a new column to the products table."""
        self.db_cursor.execute(f"""
            ALTER TABLE products
            ADD {column_name} {column_type};
        """)

    def delete_column(self, column_name: str):
        """Delete a column from the products table."""
        self.db_cursor.execute(f"""
            ALTER TABLE products
            DROP COLUMN {column_name};
        """)

    def add_records_from_file(self, filename: str):
        """Add records by reading from a CSV file."""
        with open(filename) as f:
            lines = [line for line in f]
            for line in lines:
                elements = line.split(',')
                self.add_new_record(elements[0], int(elements[1]), elements[2], float(elements[3]))

    def add_new_record(self, name: str, quantity: int, product_type: str, price_per_unit: float):
        """Add a new record to the products table."""
        self.db_cursor.execute(f"""
            INSERT INTO products(name, quantity, type, price)
            VALUES ('{name}', {quantity}, '{product_type}', {price_per_unit});
        """)

    def change_product_price(self, name: str, new_price_per_unit: float):
        """Change the price per unit of the specified record in the products table."""
        self.db_cursor.execute(f"""
            UPDATE products
            SET price = {new_price_per_unit}
            WHERE name = '{name}';
        """)

    def delete_product(self, index: int):
        """Delete the specified product from in the products table."""
        self.db_cursor.execute(f"""
            DELETE FROM products
            WHERE id = {index};
        """)

    def print_all_records(self):
        """Print all the records in the products table."""
        print('\nproducts:')
        self.db_cursor.execute("""
            SELECT *
            FROM products;
        """)
        table = from_db_cursor(self.db_cursor)
        print(table)

    def print_table_schema(self):
        """Print the schema for the products table."""
        print('\nproducts table schema:')
        self.db_cursor.execute("""
            SELECT column_name, data_type
            FROM information_schema.columns
            WHERE table_name = 'products';
        """)
        table = from_db_cursor(self.db_cursor)
        print(table)

    def get_product_id(self, name: str) -> int:
        """Return the ID of the specified product in the products table."""
        self.db_cursor.execute(f"""
            SELECT id
            FROM products
            WHERE name = '{name}';
        """)
        index = self.db_cursor.fetchone()
        return index[0]  # fetchone() returns a tuple, but only the first element is needed
