from database import DatabaseContextManager
from models import Products


# ----------------------
# Database Configuration
# ----------------------
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'learn-sql'
DATABASE_USER = 'postgres'
DATABASE_PASSWORD = 'tomatoes'


# ------------
# Main Program
# ------------

if __name__ == '__main__':
    with DatabaseContextManager(DATABASE_HOST, DATABASE_NAME, DATABASE_USER, DATABASE_PASSWORD) as db:
        db.print_database_version()

        # Create the model
        products = Products(db.db_cursor)

        # Drop all the tables
        products.drop_table()

        # Create the `products` table schema
        products.create_table_schema()
        products.print_table_schema()

        # Read all the data from the input file and add to the database
        products.add_records_from_file('part2/part2_data.csv')
        products.print_all_records()

        # ---------
        # FILTERING
        # ---------

        # Print all the herbs in the `products` table
        db.execute_sql_query("""
            SELECT *
            FROM products
            WHERE type = 'herb';
        """)

        # Print all the products that are not vegetables in the `products` table
        db.execute_sql_query("""
            SELECT *
            FROM products
            WHERE type <> 'vegetable';
        """)

        # Find the number of records in the `products` table
        db.execute_sql_query("""
            SELECT COUNT(*)
            FROM products;
        """)

        # Find the number of vegetables in the `products` table
        db.execute_sql_query("""
            SELECT COUNT(*) AS num_of_veggies
            FROM products
            WHERE type = 'vegetable';
        """)

        # Find the total number of fruits in the `products` table
        db.execute_sql_query("""
            SELECT COUNT(*) AS num_of_fruits
            FROM products
            WHERE type = 'fruit';
        """)

        # Find the distinct number of fruits in the `products` table
        db.execute_sql_query("""
            SELECT COUNT(DISTINCT name) AS num_of_distinct_fruit
            FROM products
            WHERE type = 'fruit';
        """)

        # Print all the distinct fruits in the `products` table
        db.execute_sql_query("""
            SELECT DISTINCT name
            FROM products
            WHERE type = 'fruit';
        """)

        # Print all the herbs in the `products` table with a limit of 3 records
        db.execute_sql_query("""
            SELECT name, quantity, price
            FROM products
            WHERE type = 'herb'
            LIMIT 3;
        """)

        # Print all the vegetables that cost more than $2.00
        db.execute_sql_query("""
            SELECT name, price
            FROM products
            WHERE type = 'vegetable' AND price > 2.00;
        """)

        # Print all the vegetables and herbs (Part I)
        db.execute_sql_query("""
            SELECT *
            FROM products
            WHERE type = 'vegetable' OR type = 'herb';
        """)

        # Print all the vegetables and herbs (Part II)
        db.execute_sql_query("""
            SELECT *
            FROM products
            WHERE type IN ('vegetable', 'herb');
        """)

        # Print all the fruits that have a quantity between 30 and 40 (inclusive)
        db.execute_sql_query("""
            SELECT *
            FROM products
            WHERE type = 'fruit' AND quantity BETWEEN 30 and 40;
        """)

        # Print all lettuce products
        db.execute_sql_query("""
            SELECT name, price, quantity
            FROM products
            WHERE name LIKE '%lettuce%';
        """)

        # Print all the berries
        db.execute_sql_query("""
            SELECT name, price, quantity
            FROM products
            WHERE name LIKE '%berries';
        """)

        # Print all the products that start with 'b'
        db.execute_sql_query("""
            SELECT *
            FROM products
            WHERE name LIKE 'b%';
        """)

        # Print the number of products that do not start with 't'
        db.execute_sql_query("""
            SELECT COUNT(*) AS products_not_starting_with_t
            FROM products
            WHERE name NOT LIKE 't%';
        """)

        # Print the average quantity of fruits
        db.execute_sql_query("""
            SELECT AVG(quantity) AS average_quantity_fruits
            FROM products
            WHERE type = 'fruit';
        """)

        # Print the average quantity of fruits rounded to one decimal place
        db.execute_sql_query("""
            SELECT ROUND(AVG(quantity), 1) AS average_quantity_fruits
            FROM products
            WHERE type = 'fruit';
        """)

        # Print the average price of vegetables
        db.execute_sql_query("""
            SELECT AVG(price) AS average_vegetable_price
            FROM products
            WHERE type = 'vegetable';
        """)

        # Print the average price of vegetables rounded to the nearest cent
        # NOTE: In PostreSQL, the ROUND statement can only accept a precision
        #       when using the `numeric` data type.  Therefore, CAST the average
        #       price to a `numeric` before calling ROUND.
        db.execute_sql_query("""
            SELECT ROUND( CAST(AVG(price) as numeric), 2 ) AS average_vegetable_price
            FROM products
            WHERE type = 'vegetable';
        """)

        # Print the total quantity of herbs in stock
        db.execute_sql_query("""
            SELECT SUM(quantity) AS total_quantity_herbs
            FROM products
            WHERE type = 'herb';
        """)

        # Print the lowest price of any vegetable
        db.execute_sql_query("""
            SELECT MIN(price)
            FROM products
            WHERE type = 'vegetable';
        """)

        # Print the highest price of any fruit
        db.execute_sql_query("""
            SELECT MAX(price)
            FROM products
            WHERE type = 'fruit';
        """)

        # -------
        # SORTING
        # -------

        # Print the herbs ordered by the price (lowest to highest)
        # NOTE: ORDER BY sorts the records in ascending (ASC) order from lowest to highest
        db.execute_sql_query("""
            SELECT *
            FROM products
            WHERE type = 'herb'
            ORDER BY price;
        """)

        # Print the herbs ordered by the price (highest to lowest)
        db.execute_sql_query("""
            SELECT *
            FROM products
            WHERE type = 'herb'
            ORDER BY price DESC;
        """)

        # --------
        # GROUPING
        # --------

        # Print the number of each type of product in the `products` table
        db.execute_sql_query("""
            SELECT type, COUNT(id) AS num_of_products
            FROM products
            GROUP BY type;
        """)

        # Print the average price of each type of product in the `products` table
        db.execute_sql_query("""
            SELECT type, ROUND(CAST(AVG(price) as numeric), 2) AS avg_price
            FROM products
            GROUP BY type;
        """)

        # Print find the count of each type of product that has at least 10 items in the `products` table
        db.execute_sql_query("""
            SELECT type, COUNT(id)
            FROM products
            GROUP BY type
            HAVING COUNT(id) > 10;
        """)
