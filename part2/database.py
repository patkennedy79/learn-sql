"""Contains the classes for interacting with a PostgreSQL database.

The context manager should be used to ensure that the database connection
is opened and closed properly.  Example:

    with DatabaseContextManager(HOST, NAME, USER, PASSWORD) as db:
        db.print_database_version()
"""
import psycopg2
from prettytable import from_db_cursor


class Database:
    """Class for interacting with a PostgreSQL database."""
    def __init__(self, host: str, name: str, user: str, password: str):
        """Initialize the configuration for connecting to a PostgreSQL database."""
        self.database_host = host
        self.database_name = name
        self.database_user = user
        self.database_password = password
        self.db_connection = None
        self.db_cursor = None

    def open_database_connection(self):
        """Connect to the PostgreSQL database."""
        print(f'Connecting to the PostgreSQL database ({self.database_name})...')

        # The `db_connection` object encapsulates a database session.  It is used to:
        #   1. create a new cursor object
        #   2. terminate database transactions using either:
        #      - commit() - success condition where database updates are saved (persist)
        #      - rollback() - failure condition where database updates are discarded
        self.db_connection = psycopg2.connect(
            host=self.database_host,
            database=self.database_name,
            user=self.database_user,
            password=self.database_password
        )

        # The `db_cursor` object allows the following interactions with the database:
        #   1. send SQL commands to the database using `execute()` or `executemany()`
        #   2. retrieve data from the database using `fetchone()`, `fetchmany()`, or `fetchall()`
        self.db_cursor = self.db_connection.cursor()

    def print_database_version(self):
        """Print the database version information."""
        self.db_cursor.execute('SELECT version();')
        print(f'PostgreSQL database version: {self.db_cursor.fetchone()}')

    def close_database_connection(self):
        """Close the database connection."""
        print(f'\nClosing connection to the PostgreSQL database ({self.database_name}).')
        self.db_cursor.close()
        self.db_connection.close()

    def commit(self):
        """Commit the changes to the database to make them persistent."""
        self.db_connection.commit()

    def rollback(self):
        """Discard the changes to the database to revert to the previous state."""
        self.db_connection.rollback()

    def execute_sql_query(self, query: str):
        """Execute a SQL query and print the result."""
        self.db_cursor.execute(query)
        table = from_db_cursor(self.db_cursor)
        print(table)


class DatabaseContextManager:
    """Context Manager for interacting with a PostgreSQL database."""
    def __init__(self, host: str, name: str, user: str, password: str):
        self.database = Database(host, name, user, password)

    def __enter__(self):
        self.database.open_database_connection()
        return self.database

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.database.commit()
        self.database.close_database_connection()
