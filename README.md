# Learn SQL with Python

## Overview

This project is for learning about SQL (Structured Query Language) by using Python to create applications to 
interact with a PostgeSQL relational database.

The project is the source code for a 3-part series on my blog called "Learn SQL with Python":
[https://www.patricksoftwareblog.com/index.html](https://www.patricksoftwareblog.com/index.html)

Parts:
1. SQL Fundamentals
2. Filtering, Sorting, and Grouping
3. Joins and Set Operators

## Running the Python Scripts

The Python scripts in this project require that a PostgeSQL database is running locally, ideally within a Docker container. 

### Docker - Postgres

After installing Docker Desktop, check that Docker is installed and running:

```sh
$ docker --version
Docker version 20.10.22, build 3a2c30b
```

Pull down the latest Postgres image:

```sh
$ docker pull postgres
```

Start a new container running a Postgres server and creating a new database:

```sh
$ docker run --name learn-sql-with-python -e POSTGRES_PASSWORD=tomatoes -e POSTGRES_DB=learn-sql -p 5432:5432 -d postgres
```

Here are all the options used:

* `--name` - name of the Postgres container
* `-e` - environment variable applicable to the Postgres container:
    * 'POSTGRES_PASSWORD' - database password (*required*)
    * 'POSTGRES_DB' - database name that gets created when the container starts (*optional*, default database created is `postgres`)
* `-p` - port mapping of the host port on your computer that maps to the PostgreSQL container port inside the container
* `-d` - run in daemon mode (container keeps running in the background)

To check that the PostgreSQL container is running:

```sh
$ docker ps
CONTAINER ID   IMAGE     ... STATUS         PORTS                    NAMES
9d5784e74bea   postgres  ... Up 2 seconds   0.0.0.0:5432->5432/tcp   learn-sql-with-python
```

To stop the PostgreSQL container, copy the "Container ID" from the `docker ps` command:

```sh
$ docker stop <CONTAINER ID>
```

### Python Scripts

There are Python scripts (that interface with the PostreSQL database) in the "part1", "part2", and "part3" folders.  For
example, start in the top-level folder and run the script in the "part1" folder:

```sh
$ python3 part1/part1.py
```

## Tutorial

### Part 1 - SQL Fundamentals

The first part of the "Learn SQL with Python" series demonstrates how to connect with a PostreSQL database.  Additionally,
this part demonstrates the SQL statements for the basic CRUD operations:

* **C**reate - add new records to a table in the database
* **R**ead - retrieving or querying data from the database records
* **U**pdate - modifying existing records in the database
* **D**elete - removing records from the database

### Part 2 - Filtering, Sorting, and Grouping

...

### Part 3 - Joins and Set Operators

...
