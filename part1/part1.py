"""Script for working with a PostgeSQL database.

This script demonstrates how to create a context manager for
opening/closing a connection to a PostgreSQL database, as well
as the SQL statements for the basic CRUD operations:
    * Create - add new records to a table in the database
    * Read - retrieving or querying data from the database records
    * Update - modifying existing records in the database
    * Delete - removing records from the database
"""
import psycopg2
from prettytable import from_db_cursor


# ----------------------
# Database Configuration
# ----------------------
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'learn-sql'
DATABASE_USER = 'postgres'
DATABASE_PASSWORD = 'tomatoes'


# --------
# Database
# --------

class Database:
    """Class for interacting with a PostgreSQL database."""
    def __init__(self, host: str, name: str, user: str, password: str):
        self.database_host = host
        self.database_name = name
        self.database_user = user
        self.database_password = password
        self.db_connection = None
        self.db_cursor = None

    def open_database_connection(self):
        """Connect to the PostgreSQL database."""
        print(f'Connecting to the PostgreSQL database ({self.database_name})...')

        # The `db_connection` object encapsulates a database session.  It is used to:
        #   1. create a new cursor object
        #   2. terminate database transactions using either:
        #      - commit() - success condition where database updates are saved (persist)
        #      - rollback() - failure condition where database updates are discarded
        self.db_connection = psycopg2.connect(
            host=self.database_host,
            database=self.database_name,
            user=self.database_user,
            password=self.database_password
        )

        # The `db_cursor` object allows the following interactions with the database:
        #   1. send SQL commands to the database using `execute()` or `executemany()`
        #   2. retrieve data from the database using `fetchone()`, `fetchmany()`, or `fetchall()`
        self.db_cursor = self.db_connection.cursor()

    def print_database_version(self):
        """Print the database version information."""
        self.db_cursor.execute('SELECT version();')
        print(f'PostgreSQL database version: {self.db_cursor.fetchone()}')

    def close_database_connection(self):
        """Close the database connection."""
        print(f'\nClosing connection to the PostgreSQL database ({self.database_name}).')
        self.db_cursor.close()
        self.db_connection.close()

    def commit(self):
        """Commit the changes to the database to make them persistent."""
        self.db_connection.commit()

    def rollback(self):
        """Discard the changes to the database to revert to the previous state."""
        self.db_connection.rollback()


# ---------------
# Context Manager
# ---------------

class DatabaseContextManager:
    """Context Manager for interacting with a PostgreSQL database."""
    def __init__(self, host: str, name: str, user: str, password: str):
        self.database = Database(host, name, user, password)

    def __enter__(self):
        self.database.open_database_connection()
        return self.database

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.database.commit()
        self.database.close_database_connection()


# --------------
# Database Table
# --------------
class Products:
    """Class for working with the products table in the PostgreSQL database."""

    def __init__(self, db_cursor):
        self.db_cursor = db_cursor

    def create_table_schema(self):
        """Create the schema for the produce table."""

        # For this script, drop the products table if it exists, as this
        # table is being re-created on each call to this script
        self.db_cursor.execute('DROP TABLE IF EXISTS products;')

        # Create the schema (i.e. columns) for the products table
        # NOTE: The keyword SERIAL is used in PostgreSQL for an
        #       auto-incrementing primary key.  In other relational
        #       database systems, this would be "PRIMARY KEY AUTO_INCREMENT".
        self.db_cursor.execute("""
            CREATE TABLE products(
                id SERIAL PRIMARY KEY,
                name VARCHAR(255) NOT NULL,
                quantity INT,
                type VARCHAR(50),
                price FLOAT
            );
        """)

    def add_column(self, column_name: str, column_type: str):
        """Add a new column to the products table."""
        self.db_cursor.execute(f"""
            ALTER TABLE products
            ADD {column_name} {column_type};
        """)

    def delete_column(self, column_name: str):
        """Delete a column from the products table."""
        self.db_cursor.execute(f"""
            ALTER TABLE products
            DROP COLUMN {column_name};
        """)

    def add_new_record(self, name: str, quantity: int, product_type: str, price_per_unit: float):
        """Add a new record to the products table."""
        self.db_cursor.execute(f"""
            INSERT INTO products(name, quantity, type, price)
            VALUES ('{name}', {quantity}, '{product_type}', {price_per_unit});
        """)

    def change_product_price(self, name: str, new_price_per_unit: float):
        """Change the price per unit of the specified record in the products table."""
        self.db_cursor.execute(f"""
            UPDATE products
            SET price = {new_price_per_unit}
            WHERE name = '{name}';
        """)

    def delete_product(self, index: int):
        """Delete the specified product from in the products table."""
        self.db_cursor.execute(f"""
            DELETE FROM products
            WHERE id = {index};
        """)

    def print_all_records(self):
        """Print all the records in the products table."""
        self.db_cursor.execute("""
            SELECT *
            FROM products;
        """)
        print('\nproducts:')
        for row in self.db_cursor.fetchall():
            print(row)

    def print_all_records_v2(self):
        """Print all the records in the products table."""
        print('\nproducts:')
        self.db_cursor.execute("""
            SELECT *
            FROM products;
        """)
        table = from_db_cursor(self.db_cursor)
        print(table)

    def print_table_schema(self):
        """Print the schema for the products table."""
        print('\nproducts table schema:')
        self.db_cursor.execute("""
            SELECT column_name, data_type
            FROM information_schema.columns
            WHERE table_name = 'products';
        """)
        table = from_db_cursor(self.db_cursor)
        print(table)

    def get_product_id(self, name: str) -> int:
        """Return the ID of the specified product in the products table."""
        self.db_cursor.execute(f"""
            SELECT id
            FROM products
            WHERE name = '{name}';
        """)
        index = self.db_cursor.fetchone()
        return index[0]  # fetchone() returns a tuple, but only first element is needed


# ------------
# Main Program
# ------------

if __name__ == '__main__':
    with DatabaseContextManager(DATABASE_HOST, DATABASE_NAME, DATABASE_USER, DATABASE_PASSWORD) as db:
        db.print_database_version()

        # Create the `products` table schema
        products = Products(db.db_cursor)
        products.create_table_schema()
        products.print_table_schema()

        # Add records to the products table
        products.add_new_record('banana', 30, 'fruit', 0.79)
        products.add_new_record('cantaloupe', 12, 'fruit', 3.49)
        products.add_new_record('carrot', 43, 'vegetable', 0.39)
        products.add_new_record('tomato', 17, 'vegetable', 1.19)
        products.print_all_records()
        products.print_all_records_v2()

        # Change the price of tomatoes
        products.change_product_price('tomato', 1.56)
        products.print_all_records_v2()

        # Delete carrots from the table
        carrot_id = products.get_product_id('carrot')
        products.delete_product(carrot_id)
        products.print_all_records_v2()
